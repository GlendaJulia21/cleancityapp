import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';
import MapsScreen from '../screens/MapsScreen';
import Reports from '../screens/ReportsScreen';

const Drawer = createNativeStackNavigator();

const DrawerScreenStack = () => {
  return (
    <NavigationContainer>
      <Drawer.Navigator initialRouteName="Maps">
        <Drawer.Screen name="Maps" component={MapsScreen}/>
        <Drawer.Screen name="Reports" component={Reports}/>
      </Drawer.Navigator>
    </NavigationContainer>
  )
}

export default DrawerScreenStack