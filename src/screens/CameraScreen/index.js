import * as tf from '@tensorflow/tfjs';
import {bundleResourceIO, cameraWithTensors} from '@tensorflow/tfjs-react-native';
import { View, Text, StyleSheet, Dimensions, Button} from 'react-native';
import React, {useEffect, useRef, useState} from 'react';
import { Camera } from 'expo-camera';
import { useNavigation } from '@react-navigation/native';
const TensorCamera = cameraWithTensors(Camera);

const CAM_PREVIEW_WIDTH = Dimensions.get('window').width;
const CAM_PREVIEW_HEIGHT = CAM_PREVIEW_WIDTH/(9/16);

const OUTPUT_TENSOR_WIDTH = 270;
const OUTPUT_TENSOR_HEIGHT = 480;

const CameraScreen = () => {
    const navigation = useNavigation();
    const onSignInPressed = (data) => {
        console.log(data);
        navigation.navigate('Home');
      };
    const [tfReady, setTfReady] = useState(false);
    const [model, setModel] = useState();
    const [esTacho, setEsTacho] = useState(null);
    const [respuesta, setRespuesta] = useState('No es un Tacho!');
    const [num, setNum] = useState();
    const [num2, setNum2] = useState();
    const rafId = useRef(null);
    useEffect(() => {
        rafId.current = null;
        async function prepare () {
        await Camera.requestCameraPermissionsAsync();
        await tf.ready();
        const modelJson = require('./../../../model/model.json');
        const modelWeights = require('./../../../model/weights.bin');

        const model = await tf.loadLayersModel(bundleResourceIO(modelJson, modelWeights));
        setModel(model);
        setTfReady(true);
        console.log('Readyyy! :3');
      }
      prepare();
    }, []);

    useEffect(()=>{
        return ()=>{
            if (rafId.current != null && rafId.current !== 0){
                cancelAnimationFrame(rafId.current);
                rafId.current = 0;
            }
        };
    }, []);

    const handleCameraStream = async (images, updatePreview, gl) => {
        console.log('La camara esta lista!');

        const loop = async()=>{
            if (rafId.current === 0) {
                return;
            }
            tf.tidy(()=>{
                const imageTensor = images.next().value.expandDims(0).div(127.5).sub(1);
                const f = (OUTPUT_TENSOR_HEIGHT - OUTPUT_TENSOR_WIDTH) / 2 / OUTPUT_TENSOR_HEIGHT;
                const cropped = tf.image.cropAndResize(
                    imageTensor, 
                    tf.tensor2d([f, 0, 1-f, 1], [1, 4]),
                    [0], 
                    [224, 224]
                );
                const result = model.predict(cropped);
                const logits = result.dataSync(); //0.0896526.2568963
                setNum(logits[0]);
                setNum2(logits[1]);

                if (logits) {
                    if (Number.isInteger(logits[0])) {
                        setEsTacho(true);
                        setRespuesta('Es un tacho vacío!');
                    } else if (Number.isInteger(logits[1])){
                        setEsTacho(true);
                        setRespuesta('Es un tacho lleno!');
                    } else {
                        setRespuesta('No es un tacho!');
                        setEsTacho(false);
                    }
                    
                }else {
                    setRespuesta('No es un tacho!');
                    setEsTacho(null);
                }
            });
            
            rafId.current = requestAnimationFrame(loop);
        };
        loop();
    };
    
    if (!tfReady) {
        return (<View style={styles.loadingMsg}>
            <Text>Loading...</Text>
        </View>);
    } else {
        return (
            <View style={styles.container}>
            <Button title='Volver' onPress={onSignInPressed}/>
              <TensorCamera
              style={styles.camera}
              autorender={true}
              type={Camera.Constants.Type.back}
              resizeWidth={OUTPUT_TENSOR_WIDTH}
              resizeHeight={OUTPUT_TENSOR_HEIGHT}
              resizeDepth={3}
              onReady={handleCameraStream}
              />
              <View style={ esTacho ? styles.resultContainerEsTacho : styles.resultContainerNoesTacho}>
                <Text style={styles.resultText}>
                    {respuesta}
                </Text>
              </View>
            </View>
          );
    }
  
}

const styles = StyleSheet.create({
    container: {
        position: 'relative',
        width: CAM_PREVIEW_WIDTH,
        height: CAM_PREVIEW_HEIGHT,
        marginTop: Dimensions.get('window').height / 2 - CAM_PREVIEW_HEIGHT / 2,
    },

    camera: {
        width: '100%',
        height: '100%',
        zIndex: 1,
    },

    loadingMsg: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',

    },

    resultContainerEsTacho: {
        position: 'absolute',
        top: 0,
        left: 0,
        zIndex: 100,
        padding: 20,
        borderRadius: 8,
        backgroundColor: '#00aa00',
    },

    resultContainerNoesTacho: {
        position: 'absolute',
        top: 0,
        left: 0,
        zIndex: 100,
        padding: 20,
        borderRadius: 8,
        backgroundColor: '#aa0000',
    },

    resultText: {
        fontSize: 30,
        color: 'white',
    },
});

export default CameraScreen;