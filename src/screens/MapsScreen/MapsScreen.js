import { View, Text, StyleSheet, Dimensions, Button, Image,TouchableOpacity, Animated } from 'react-native'
import React, {useState, useEffect, useRef} from 'react'
import CustomButton from '../../components/CustomButton';
import { useNavigation } from '@react-navigation/native';
import MapView, {Marker} from 'react-native-maps';
//import DrawerScreenStack from '../../components/DrawerScreenStack';
import * as Location from 'expo-location';
import menu from '../../../assets/menu.png';

const MapsScreen = ({showMenu, setShowMenu, offsetValue, scaleValue}) => {
  //const offsetValue = useRef(new Animated.Value(0)).current;
	//const scaleValue = useRef(new Animated.Value(1)).current;
  //const [showMenu, setShowMenu] = useState(false);
  const navigation = useNavigation();
      const onCameraPressed = () => {
        navigation.navigate('Camera');
      };
    const [mapRegion, setMapRegion] = useState({
        latitude: -16.4285408,
        longitude: -71.5185263,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
    });
    const userLocation = async () => {
        let {status} = await Location.requestForegroundPermissionsAsync();
        if (status !== 'granted') {
            setErrorMsg('El permiso de Localización fue denegado.');
        }
        let location = await Location.getCurrentPositionAsync({enableHighAccuracy: true});
        setMapRegion({
            latitude: location.coords.latitude,
            longitude: location.coords.longitude,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
        });
        console.log(location.coords.latitude, location.coords.longitude);

    }

    const markers = {
        markers: [{title: '3568',coordinates: {latitude: -16.3360023, longitude: -71.5451877},img: '../../img/lleno.png',},
        {title: '2563',coordinates: {latitude: -16.328845,longitude:  -71.539533}, img: '../../img/lleno.png',},
        {title: '1691',coordinates: {latitude: -16.3253307,longitude: -71.5429843}, img: '../../img/lleno.png',},
        {title: '2443',coordinates: {latitude: -16.3275459,longitude: -71.5426524}, img: '../../img/lleno.png',},
        {title: '9852',coordinates: {latitude: -16.3291355,longitude: -71.5422731}, img: '../../img/lleno.png',},
        {title: '2486',coordinates: {latitude: -16.3299312,longitude: -71.5429099}, img: '../../img/lleno.png',},
        {title: '8552',coordinates: {latitude: -16.330759,longitude: -71.5429516}, img: '../../img/lleno.png',},
        {title: '7514',coordinates: {latitude: -16.3276677,longitude: -71.540666}, img: '../../img/lleno.png',},
        {title: '6452',coordinates: {latitude: -16.330340, longitude: -71.540716}, img: '../../img/lleno.png',},
        {title: '2578',coordinates: {latitude: -16.3276677, longitude: -71.540666}, img: '../../img/lleno.png',},
        {title: '5456',coordinates: {latitude: -16.3342892, longitude: -71.5428864}, img: '../../img/lleno.png',}]
      }
    useEffect(() => {
        userLocation();
    }, []);
  return (
    <View>
        <TouchableOpacity onPress={() => {
					Animated.timing(scaleValue, {
						toValue: showMenu ? 1 : 0.88,
						duration: 300,
						useNativeDriver: true
					})
					.start()

					Animated.timing(offsetValue, {
						toValue: showMenu ? 0 :220,
						duration: 300,
						useNativeDriver: true
					})
					.start()
					setShowMenu(!showMenu);
				}}>
					<Image source={menu} style={{
						width: 20,
						height: 20,
						tintColor: 'black',
						marginTop: 40,
						paddingHorizontal: 15
					}}>
						
					</Image>
					<Text style={{
						fontSize: 30,
						fontWeight: 'bold',
						color: 'black',
						paddingTop: 20,

					}}>Mapa</Text>
          <View>
					<MapView style={styles.map} region={mapRegion}>
            <Marker coordinate={mapRegion} title='Marker'/>
            {markers.markers.map(marker => (
                <Marker 
                coordinate={marker.coordinates}
                title={marker.title}>
                <Image source={require('../../img/contenedor.png')} style={{height: 35, width:35 }} />
                </Marker>
            ))}
        </MapView>
        <Button title='Obtener ubicación' onPress={userLocation}/>
        </View>
				</TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  map: {
      width: Dimensions.get('window').width-25,
      height: Dimensions.get('window').height-250,
  }
});


export default MapsScreen;