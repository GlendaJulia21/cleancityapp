import React, {useState} from 'react';
import { 
  View, 
  Text, 
  StyleSheet, 
  Image, 
  useWindowDimensions,
  ScrollView,
  TextInput 
} from 'react-native';
import Logo from '../../../assets/images/Logo.png';
import CustomInput from '../../components/CustomInput';
import CustomButton from '../../components/CustomButton';
import SocialSignInButtons from '../../components/SocialSignInButtons';
import { useNavigation } from '@react-navigation/native';
import { useForm} from 'react-hook-form';

const SignInScreen = () => {

  const {height} = useWindowDimensions();
  const navigation = useNavigation();

  const {
    control, 
    handleSubmit,
    formState: {errors} } = useForm();

  const onSignInPressed = (data) => {
    console.log(data);
    navigation.navigate('Home');
  };
  const onForgotPasswordPressed = () => {
    navigation.navigate('ForgotPassword');
  };

  const onSignUpPressed = () => {
    navigation.navigate('SignUp');
  };

  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.root}>
        <Image 
          source={Logo} 
          styles={[styles.logo, {height: height * 0.3}]} 
          resizeMode="contain"
        />
        <CustomInput 
          name="username"
          placeholder="Usuario" 
          control={control}
          rules={{required: 'Usuario es un campo obligatorio'}}
        />
        <CustomInput 
          name="password"
          placeholder="Contraseña"
          secureTextEntry
          control={control}
          rules={{
            required: 'La Contraseña es obligatorio', 
            minLength: {
              value: 3, 
              message: 'La Contraseña debería tener como mínimo 3 caracteres.',
            },
            }}
          /> 
        <CustomButton text="Iniciar Sesion" onPress={handleSubmit(onSignInPressed)}/>

        {/*<CustomButton 
          text="Forgot password?" 
          onPress={onForgotPasswordPressed}
          type="TERTIARY"
          />*/}

        <SocialSignInButtons/>

        <CustomButton 
          text="¿No tiene una cuenta? Crea una" 
          onPress={onSignUpPressed}
          type="TERTIARY"
        />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  root: {
    alignItems: 'center',
    padding: 20,
  },
  logo: {
    width:'70%',
    maxWidth: 300,
    maxHeight: 200,
  },
});

export default SignInScreen;