import React, {useState} from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native';
import CustomInput from '../../components/CustomInput';
import CustomButton from '../../components/CustomButton';
import SocialSignInButtons from '../../components/SocialSignInButtons';
import { useNavigation } from '@react-navigation/native';
import { useForm} from 'react-hook-form';

const SignUpScreen = () => {
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [passwordRepeat, setPasswordRepeat] = useState('');

  const navigation = useNavigation();

  const {
    control, 
    handleSubmit,
    watch,
    formState: {errors} } = useForm();
  
  const pwd = watch('password');

  const onRegisterPressed = (data) => {
    console.log(data);
    navigation.navigate('Home');
  };

  const onSignInPressed = () => {
    navigation.navigate('SignIn');
  };

  const onTermsOfUsePressed = () => {
    console.warn("Terms of Use!");
  };

  const onPrivacyPressed = () => {
    console.warn("Privacy Policy!");
  };

  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.root}>
        <Text style={styles.title}>Crear una cuenta</Text>
        <CustomInput 
          name="username"
          placeholder="Usuario" 
          control={control}
          rules={{required: 'Usuario es un campo obligatorio'}}
        />
        <CustomInput 
          name="email" 
          placeholder="Correo" 
          control={control}
          rules={{required: 'Correo es obligatorio'}}
        />
        <CustomInput 
          name="password"
          placeholder="Contraseña"
          secureTextEntry
          control={control}
          rules={{
            required: 'La Contraseña es obligatorio', 
            minLength: {
              value: 3, 
              message: 'La Contraseña debería tener como mínimo 3 caracteres.',
            },
            }}
          /> 
        <CustomInput 
          name="passwordRepeat"
          placeholder="Repetir Contraseña" 
          secureTextEntry
          control={control}
          rules={{
            validate: value => value == pwd || 'Las contraseñas no son iguales'
            }}
        />
        <CustomButton text="Registrar" onPress={handleSubmit(onRegisterPressed)}/>

        <Text style={styles.text}>
        Al registrarse, confirma que acepta nuestros {' '}
          <Text style={styles.link} onPress={onTermsOfUsePressed}>Términos de Uso</Text> and{' '}
          <Text style={styles.link} onPress={onPrivacyPressed}>Políticas de privacidad</Text>
        </Text>

        <SocialSignInButtons/>

        <CustomButton 
          text="¿Ya tiene una cuenta? Inicie sesión" 
          onPress={onSignInPressed}
          type="TERTIARY"
        />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  root: {
    alignItems: 'center',
    padding: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#051C60',
    margin: 10,
  },
  text: {
    color: 'gray',
    marginVertical: 10,
  },
  link: {
    color: '#FD8075'
  }
});

export default SignUpScreen;