import { StatusBar } from 'expo-status-bar';
//import {Animated} from 'react-native-web';
import { SafeAreaView, StyleSheet, Image, View, Text, TouchableOpacity, Animated } from 'react-native';
import profile from '../../../assets/profile.png';
import mapicon from '../../../assets/map.png';
import alert from '../../../assets/alert.png';
import settings from '../../../assets/settings.png';
import logout from '../../../assets/logout.png';
import menu from '../../../assets/menu.png';
import close from '../../../assets/close.png';
import photo from '../../../assets/perfil.jpg';
import { useRef, useState } from 'react';
import { useNavigation } from '@react-navigation/native';
import MapsScreen from '../MapsScreen/MapsScreen.js'
import ReportsScreen from '../ReportsScreen/ReportsScreen.js'

const HomeScreen = () => {
    const navigation = useNavigation();
    const onSignInPressed = () => {
        navigation.navigate('SignIn');
      };
    const [currentTab, setCurrentTab] = useState("Mapa");
	const [showMenu, setShowMenu] = useState(false);

	const offsetValue = useRef(new Animated.Value(0)).current;
	const scaleValue = useRef(new Animated.Value(1)).current;
	const closeButtonOffset = useRef(new Animated.Value(0)).current;

    return (
		<SafeAreaView style={styles.container}>
			<View style={{justifyContent: 'flex-start', padding:15}}>
				<Image source={profile} style={{
					width:60,
					height: 60,
					borderRadius: 10,
					marginTop: 8
				}}></Image>
				<Text style={{
					fontSize: 20,
					fontWight: 'bold',
					color: 'white',
					marginTop: 20
				}}>Glenda Julia</Text>
				<TouchableOpacity>
					<Text style={{
						marginTop: 10,
						color: 'white',

					}}>Ver Perfil</Text>
				</TouchableOpacity>

				<View style={{flexGrow: 1, marginTop: 50}}>
					{
						//Tab bar buttons
					}
					{TabButton(currentTab, setCurrentTab, "Mapa", mapicon)}
					{TabButton(currentTab, setCurrentTab, "Reportes", alert)}
					{TabButton(currentTab, setCurrentTab, "Configuracion", settings)}
				</View>

				<View>
					{TabButton(currentTab, onSignInPressed, "Cerrar Sesión", logout)}
				</View>
				
			</View>
			{

			}
			<Animated.View style={{
				flexGrow: 1,
				backgroundColor: 'white',
				position: 'absolute',
				top: 0,
				bottom: 0,
				left: 0,
				right: 0,
				paddingHorizontal:15,
				paddingVertical: 20,
				borderRadius: showMenu ? 15 : 0,
				transform: [
					{scale:scaleValue},
					{translateX: offsetValue}
				]
			}}>

				{
					//
				}

				
                <View>
                    {ViewItem(currentTab, showMenu, setShowMenu, scaleValue, offsetValue)}
				</View>

			</Animated.View>
		</SafeAreaView>
	);
};

const ViewItem = (title, showMenu, setShowMenu, scaleValue, offsetValue) =>{
    if (title=="Mapa") {
        return (
            <MapsScreen showMenu={showMenu} setShowMenu={setShowMenu} scaleValue={scaleValue} offsetValue={offsetValue}/>
            );
    }else if (title=="Reportes"){
        return (
            <ReportsScreen showMenu={showMenu} setShowMenu={setShowMenu} scaleValue={scaleValue} offsetValue={offsetValue}/>
            );
    }else{
        return (
            <ReportsScreen showMenu={showMenu} setShowMenu={setShowMenu} scaleValue={scaleValue} offsetValue={offsetValue}/>
            );
    }
    
};

const TabButton = (currentTab, setCurrentTab, title, image) =>{
	return (
		<TouchableOpacity onPress={() =>{
			if (title=="Cerrar Sesión") {
                setCurrentTab();
			}else{
				setCurrentTab(title);
			}
		}}>
			<View style={{
				flexDirection: "row",
				alignItems: 'center',
				paddingVertical: 8,
				backgroundColor: currentTab == title ? 'white' : 'transparent',
				paddingLeft: 13,
				paddingRight:35,
				borderRadius: 8,
				marginTop: 15
			}}>
				<Image source={image} style={{
					width: 25,
					height: 25,
					tintColor: currentTab == title ? "#3EA813" : "white"

				}}></Image>
				<Text style={{
					fontSize: 15,
					fontWeight: 'bold',
					paddingLeft: 15,
					color: currentTab == title ? "#3EA813" : "white"
				}}>{title}</Text>
			</View>
		</TouchableOpacity>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#5CCB2F',
		alignItems: 'flex-start',
		justifyContent: 'flex-start',
	},
});

export default HomeScreen;