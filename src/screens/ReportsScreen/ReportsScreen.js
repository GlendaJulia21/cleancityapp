import { View, Text, TouchableOpacity, Animated, Image, Button, ScrollView, StyleSheet, FlatList } from 'react-native'
import React, { useState } from 'react'
import { useNavigation } from '@react-navigation/native';
import menu from '../../../assets/menu.png';

const ReportsScreen = ({showMenu, setShowMenu, offsetValue, scaleValue}) => {
  const navigation = useNavigation();
  const [items, setItems] = useState([{"id":1,"nombre":"Reporte 1"},{"id":2,"nombre":"Reporte 1"},{"id":3,"nombre":"Reporte 1"},{"id":4,"nombre":"Reporte 1"}])
  const onCameraPressed = () => {
    navigation.navigate('Camera');
  };
  return (
    <View>
        <TouchableOpacity onPress={() => {
					Animated.timing(scaleValue, {
						toValue: showMenu ? 1 : 0.88,
						duration: 300,
						useNativeDriver: true
					})
					.start()

					Animated.timing(offsetValue, {
						toValue: showMenu ? 0 :220,
						duration: 300,
						useNativeDriver: true
					})
					.start()
					setShowMenu(!showMenu);
				}}>
					<Image source={menu} style={{
						width: 20,
						height: 20,
						tintColor: 'black',
						marginTop: 40,
						paddingHorizontal: 15
					}}>
						
					</Image>
					<Text style={{
						fontSize: 30,
						fontWeight: 'bold',
						color: 'black',
						paddingTop: 20,

					}}>Reportes</Text>
          <View contentContainerStyle={styles.ScrollView}>
            <ToDoItem texto="Tacho Lleno" msg="El tacho esta lleno, enviar personal de recojo" visto={false}/>
            <ToDoItem texto="Falta Tacho" msg="En esta zona no hay tachos disponibles" visto={false}/>
            <ToDoItem texto="No esta un tacho!" msg="No esta el tacho en el lugar que indica el aplicativo!" visto={true}/>
            <ToDoItem texto="Tacho lleno" msg="Los perros se estan comiendo la basura" visto={true}/>
            <ToDoItem texto="Tacho roto!" msg="Hay un tacho dañado" visto={true}/>
          </View>
          
        <Button title='Reportar Incidencia' onPress={onCameraPressed}/>
				</TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  ScrollView: {
    flex:1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F8F9FD'
  },
  ItemView:{
    width: '100%',
    height:70,
    borderWidth:1,
    backgroundColor:'#ffffff',
    borderRadius:5,
    marginTop:8,
    borderColor:'#09BE00',
    justifyContent:'center'
},
Item:{
  width: '100%',
  height:70,
  borderWidth:1,
  backgroundColor:'#ffffff',
  borderRadius:5,
  marginTop:8,
  borderColor:'#BE2300',
  justifyContent:'center'
},
ToDoText:{
  marginLeft:10,
},
ToDoTextBold:{
  marginLeft:10,
  fontWeight:'bold',
},
mainView:{
  width:'100%',
  alignItems: 'center'
}
});

const ToDoItem = ({visto,texto,msg}) => {
  return (
    <View style={styles.mainView}>
      <View style={visto ? styles.ItemView : styles.Item }>
        <Text style={styles.ToDoTextBold}>Asunto: </Text><Text style={styles.ToDoText}>{texto}</Text>
        <Text style={styles.ToDoTextBold}>Mensaje: </Text><Text style={styles.ToDoText}>{msg}</Text>
      </View>
    </View>
  )
};

export default ReportsScreen;