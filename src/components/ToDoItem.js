import { View, Text,StyleSheet } from 'react-native'
import React from 'react'

const ToDoItem = (props) => {
  return (
    <View style={styles.Item}>
      <Text>ToDoItem</Text>
    </View>
  )
}

const styles = StyleSheet.create({
    Item:{
        width: '90%',
        height:46,
        borderWidth:1,
        backgroundColor:'#ffffff',
        borderRadius:5,
    }
})

export default ToDoItem