import { View, Text } from 'react-native'
import React from 'react'
import CustomButton from '../CustomButton';

const SocialSignInButtons = () => {
    
  const onSignInWithFacebook = () => {
    console.warn("Facebook!");
  };

  const onSignInWithGoogle = () => {
    console.warn("Google!");
  };

  const onSignInWithApple = () => {
    console.warn("Apple!");
  };

  return (
    <>
      <CustomButton 
          text="Iniciar sesion con Facebook" 
          onPress={onSignInWithFacebook}
          bgColor="#E7EAF4"
          fgColor="#4765A9"
        />
        <CustomButton 
          text="Iniciar sesion con Google" 
          onPress={onSignInWithGoogle}
          bgColor="#FAE9EA"
          fgColor="#DD4D44"
        />
        <CustomButton 
          text="Iniciar sesion con Apple" 
          onPress={onSignInWithApple}
          bgColor="#E3E3E3"
          fgColor="#363636"
        />
    </>
  )
}

export default SocialSignInButtons